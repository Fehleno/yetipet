package net.fehleno.yetipet;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


public class Dialog
{
	public static final Path dialogFolder = Path.of("./data/dialog");
	
	/**
	 * Prints out the content of a dialog files and then waits for the user to
	 * press enter to continue. Adds separation lines.
	 * 
	 * @param fileName
	 *     the relative path to the dialog file from data/dialog.
	 * @throws IOException
	 */
	private static void print(String fileName) throws IOException
	{
		System.out.println(Files.readString(dialogFolder.resolve(fileName)));
		TerminalHandler.awaitEnter();
		TerminalHandler.seperator();
	}
	
	public static void intro1() throws IOException
	{
		print("intro/1.txt");
	}
	
	public static void intro2() throws IOException
	{
		print("intro/2.txt");
	}
	
	public static void intro3() throws IOException
	{
		print("intro/3.txt");
	}
	
	public static void intro4() throws IOException
	{
		print("intro/4.txt");
	}
	
	public static void intro5() throws IOException
	{
		print("intro/5.txt");
	}
	
}
