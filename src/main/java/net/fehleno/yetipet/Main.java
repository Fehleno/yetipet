package net.fehleno.yetipet;


import java.io.IOException;


public class Main
{
	
	public static void main(String[] args) throws IOException
	{
		Dialog.intro1();
		Dialog.intro2();
		Dialog.intro3();
		System.out.println("Which color did the Yeti choose?");
		PonchoColor ponchoColor = TerminalHandler.optionTree(PonchoColor.class);
		System.out.println("The ponchos color is " + ponchoColor + ", that's a nice color!");
		TerminalHandler.awaitEnter();
		TerminalHandler.seperator();
		Dialog.intro4();
		Dialog.intro5();
		System.out.println("Enter the yetis name: ");
		String yetiName = TerminalHandler.readString();
		System.out.println("The yetis name is " + yetiName + "?\nThats a sweet name for a yeti!");
		
	}
	
}
