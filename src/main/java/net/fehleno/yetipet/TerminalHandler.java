package net.fehleno.yetipet;


import java.util.Scanner;


public class TerminalHandler
{
	private static Scanner scanner = new Scanner(System.in);
	
	/**
	 * @return the text the user types into the terminal.
	 */
	public static String readString()
	{
		while (true)
		{
			String input = scanner.nextLine();
			
			if (input.equals(""))
			{
				System.out.println("Invalid input, please try again.");
			}
			else
			{
				return input;
			}
		}
	}
	
	/**
	 * @return the integer the user types into the terminal.
	 */
	public static int readInt()
	{
		return Integer.parseInt(readString());
	}
	
	/**
	 * Blocks the code until the user presses enter.
	 */
	public static void awaitEnter()
	{
		System.out.println("Press enter to continue.");
		scanner.nextLine();
	}
	
	/**
	 * Prints out separation character into the terminal.
	 */
	public static void seperator()
	{
		System.out.println("-".repeat(20));
	}
	
	/**
	 * Lets the user choice between a string with options
	 * 
	 * @param options
	 *     the choices available to the user
	 * @return the choice the user made
	 */
	public static String optionTree(String... options)
	{
		int i = 1;
		for (String option : options)
		{
			System.out.println(i + ". " + option);
			i++;
			
		}
		int choice = readInt();
		return options[choice - 1];
	}
	
	public static <E extends Enum<E>> E optionTree(Class<E> enumClass)
	{
		int i = 1;
		for (E option : enumClass.getEnumConstants())
		{
			System.out.println(i + ". " + option);
			i++;
			
		}
		int choice = readInt();
		return enumClass.getEnumConstants()[choice - 1];
	}
	
}
