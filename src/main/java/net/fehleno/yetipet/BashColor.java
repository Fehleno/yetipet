package net.fehleno.yetipet;

public class BashColor
{
	// Special operations
	
	public static final String reset = create(0);
	public static final String bold = create(1);
	public static final String faint = create(2);
	public static final String italics = create(3);
	public static final String underline = create(4);
	
	// Main text colors
	
	public static final String black = create(30);
	public static final String red = create(31);
	public static final String green = create(32);
	public static final String yellow = create(33);
	public static final String blue = create(34);
	public static final String magenta = create(35);
	public static final String cyan = create(36);
	public static final String lightGray = create(37);
	public static final String gray = create(90);
	public static final String lightRed = create(91);
	public static final String lightGreen = create(92);
	public static final String lightYellow = create(93);
	public static final String lightBlue = create(94);
	public static final String lightMagenta = create(95);
	public static final String lightCyan = create(96);
	public static final String white = create(97);
	
	// Background colors
	
	public static final String bgBlack = create(30 + 10);
	public static final String bgRed = create(31 + 10);
	public static final String bgGreen = create(32 + 10);
	public static final String bgYellow = create(33 + 10);
	public static final String bgBlue = create(34 + 10);
	public static final String bgMagenta = create(35 + 10);
	public static final String bgCyan = create(36 + 10);
	public static final String bgLightGray = create(37 + 10);
	public static final String bgGray = create(90 + 10);
	public static final String bgLightRed = create(91 + 10);
	public static final String bgLightGreen = create(92 + 10);
	public static final String bgLightYellow = create(93 + 10);
	public static final String bgLightBlue = create(94 + 10);
	public static final String bgLightMagenta = create(95 + 10);
	public static final String bgLightCyan = create(96 + 10);
	public static final String bgWhite = create(97 + 10);
	
	/**
	 * Generates the escape character for the selected color or text modifier.
	 * 
	 * @param code
	 *     the bash color code.
	 * @return the control sequence character for the terminal.
	 */
	private static String create(int code)
	{
		return "\033[" + code + "m";
	}
}
