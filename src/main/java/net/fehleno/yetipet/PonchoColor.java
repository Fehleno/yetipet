package net.fehleno.yetipet;

public enum PonchoColor
{
	RED(BashColor.red),
	GREEN(BashColor.green),
	BLUE(BashColor.blue);
	
	private String terminalColor;
	
	private PonchoColor(String terminalColor)
	{
		this.terminalColor = terminalColor;
	}
	
	public String getTerminalColor()
	{
		return terminalColor;
	}
	
	@Override
	public String toString()
	{
		return terminalColor + super.toString() + BashColor.reset;
	}
}
